const axios = require('axios');

// สร้าง function สำหรับเรียกใช้งาน API และทดสอบผลลัพธ์
async function testAPI() {
    try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');
        console.log('Response from API:', response.data);
        if (response.data.length > 0) {
            console.log('API Test Passed!');
        } else {
            console.log('API Test Failed!');
        }
    } catch (error) {
        console.error('Error:', error);
    }
}

// เรียกใช้งาน function testAPI()
testAPI();

